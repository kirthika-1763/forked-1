function addBook() {
    var title = document.getElementById("title").value;
    var author = document.getElementById("author").value;
    var isbn = document.getElementById("isbn").value;

    if (title && author && isbn) {
        var bookList = document.getElementById("bookList");
        var li = document.createElement("li");
        li.innerHTML = `<strong>${title}</strong> by ${author}<br>ISBN: ${isbn}`;
        bookList.appendChild(li);

        // Clear form inputs
        document.getElementById("title").value = "";
        document.getElementById("author").value = "";
        document.getElementById("isbn").value = "";
    } else {
        alert("Please fill in all fields.");
    }
}
